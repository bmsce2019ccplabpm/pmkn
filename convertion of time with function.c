#include <stdio.h>
int input ()
{
 int hr;
 printf("Enter time in hours:\n");
 scanf("%d",&hr);
 return hr;
}
int compute(int hr)
{
 int min=hr*60;
 return min;
}
void output(int hr,int min)
{
 printf("%d hours is equal to %d minutes\n",hr,min);
}
int main()
{
 int hr = input();
 int min = compute(hr);
 output(hr,min);
 return 0;
}   